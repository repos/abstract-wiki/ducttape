user := `whoami`

build:
  cargo build

run:
  cargo run

deploy toolforge_user=user:
  scripts/deploy_to_toolforge.sh {{toolforge_user}}
