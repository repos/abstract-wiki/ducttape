#!/usr/bin/env bash

echo "Starting new environments"
curl "https://aw-gerrit-gitlab-bridge.toolforge.org/api/v0/environments/start_new"
echo
echo "Stopping old environments"
curl "https://aw-gerrit-gitlab-bridge.toolforge.org/api/v0/environments/stop_old"
echo
echo "Updating stale environments"
curl "https://aw-gerrit-gitlab-bridge.toolforge.org/api/v0/environments/update_stale"
echo