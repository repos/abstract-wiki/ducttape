#!/usr/bin/env bash

user=${1-$USER}

ssh $user@login.toolforge.org "
  set -ex
  become aw-gerrit-gitlab-bridge bash -exc \"
    cd ~/repo &&
    git pull --ff-only &&
    pwd &&
    time jsub -N build -mem 2G -sync y -cwd cargo build --release &&
    webservice stop &&
    webservice start
  \"
"
