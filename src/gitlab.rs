use std::{collections::HashMap};

use anyhow::{Context, Result};
use chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};

#[derive(Clone)]
pub struct Client {
    api_token: String,
    trigger_token: String,
    cached_pipeline_variables: HashMap<usize, Vec<PipelineVariable>>
 }

#[derive(Serialize, Debug)]
struct PipelineTriggerRequest {
    token: String,
    #[serde(rename = "ref")]
    gitlab_ref: String,
    variables: PipelineTriggerVariables,
}
#[derive(Serialize, Debug)]
struct PipelineTriggerVariables {
    #[serde(rename = "GERRIT_CHANGE_NUMBER")]
    gerrit_change_number: String,
    #[serde(rename = "WIKILAMBDA_REF")]
    wikilambda_ref: String,
    #[serde(rename = "ACTION")]
    action: String,
}

#[derive(Deserialize, Debug)]
pub struct PipelineTriggerResponse {
    web_url: String,
}

#[derive(Deserialize, Debug)]
pub struct Pipeline {
    pub id: usize,
    pub web_url: String,
    pub created_at: DateTime<Utc>,
    #[serde(default)]
    pub variables: Vec<PipelineVariable>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct PipelineVariable {
    pub key: String,
    pub value: String, 
}

impl Client {
    pub fn new(api_token: &str, trigger_token: &str) -> Client {
        Client {
          api_token: api_token.to_owned(),
          trigger_token: trigger_token.to_owned(),
          cached_pipeline_variables: HashMap::<usize, Vec<PipelineVariable>>::new()
        }
    }

    pub async fn trigger_run(&self, gerrit_change_number: usize, gerrit_ref: &str) -> Result<String, anyhow::Error> {
        self.trigger_pipeline(gerrit_change_number, "run", gerrit_ref).await
    }

    pub async fn trigger_stop(&self, gerrit_change_number: usize) -> Result<String, anyhow::Error> {
        self.trigger_pipeline(gerrit_change_number, "stop", "").await
    }

    pub async fn trigger_pipeline(&self, gerrit_change_number: usize, action: &str, gerrit_ref: &str) -> Result<String, anyhow::Error> {
        let request_url = "https://gitlab.wikimedia.org/api/v4/projects/1074/trigger/pipeline".to_string();
        let request_body = PipelineTriggerRequest {
            token: self.trigger_token.to_owned(),
            gitlab_ref: "main".to_string(),
            variables: PipelineTriggerVariables {
                gerrit_change_number: gerrit_change_number.to_string(),
                wikilambda_ref: gerrit_ref.to_owned(),
                action: action.to_owned(),
            }
        };
        let http_client = reqwest::Client::new();
        let response = http_client.post(&request_url).json(&request_body).send()
          .await.with_context(|| "failed http POST request to gitlab")?;
        if response.status() != 201 {
            anyhow::bail!("trigger pipeline request failed: {}, {}",
              response.status().as_u16(),
              response.text().await.unwrap_or("could not read body".to_owned()));
        }
        let body: PipelineTriggerResponse = response.json()
          .await.with_context(|| "failed to read response body from gitlab")?;
        Ok(body.web_url)
    }

    // paginates through all pipelines, and caches the variables for each pipeline
    pub async fn get_pipelines(&mut self) -> Result<Vec<Pipeline>, anyhow::Error> {
        let mut pipelines: Vec<Pipeline> = Vec::new();
        let mut page = 1;
        loop {
            let request_url = format!("https://gitlab.wikimedia.org/api/v4/projects/1074/pipelines?page={}", page);
            let http_client = reqwest::Client::new();
            let pipelines_resp = http_client
              .get(&request_url)
              .header("PRIVATE-TOKEN", &self.api_token)
              .send().await
              .with_context(|| "failed getting pipelines")?;

            if !pipelines_resp.status().is_success() {
                anyhow::bail!("pipelines request failed: {}, {}",
                  pipelines_resp.status().as_u16(), 
                  pipelines_resp.text().await.unwrap_or("could not read body".to_owned()));
            }

            let mut page_pipelines: Vec<Pipeline> = pipelines_resp
              .json().await.with_context(|| "failed parsing pipelines")?;
            if page_pipelines.len() == 0 {
                break;
            }
            for mut pipeline in page_pipelines.iter_mut() {
                pipeline.variables = self.get_variables_for_pipeline(pipeline.id)
                  .await.with_context(|| "get variables for pipeline failed")?
            }
            pipelines.append(&mut page_pipelines);
            page += 1;
        }
        Ok(pipelines)
    }

    // gets variables for a pipeline, and caches them
    pub async fn get_variables_for_pipeline(&mut self, pipeline_id: usize) -> Result<Vec<PipelineVariable>, anyhow::Error> {
        if let Some(hit) = self.cached_pipeline_variables.get(&pipeline_id) {
          return Ok(hit.to_owned());
        }

        let request_url = format!("https://gitlab.wikimedia.org/api/v4/projects/1074/pipelines/{}/variables", pipeline_id);
        let http_client = reqwest::Client::new();
        let pipelines_resp = http_client
          .get(&request_url)
          .header("PRIVATE-TOKEN", &self.api_token)
          .send().await.with_context(|| "failed getting pipelines")?;

        if !pipelines_resp.status().is_success() {
            anyhow::bail!("get pipelines variables request failed: {}, {}",
              pipelines_resp.status().as_u16(), 
              pipelines_resp.text().await.unwrap_or("could not read body".to_owned()));
        }

        let pipeline_variables: Vec<PipelineVariable> = pipelines_resp
          .json().await.with_context(|| "failed parsing pipeline variables")?;
        self.cached_pipeline_variables.insert(pipeline_id, pipeline_variables.to_owned());
        Ok(pipeline_variables)
    }

}