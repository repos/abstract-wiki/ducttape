use anyhow::{Context, Result};
use std::collections::HashMap;

use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Change {
    #[serde(rename = "_number")]
    pub number: usize,
    revisions: HashMap<String, GerritRevision>,
}
#[derive(Deserialize, Debug)]
struct GerritRevision {
    #[serde(rename = "ref")]
    git_ref: String,
}

impl Change {
    pub fn git_ref(&self) -> String {
        self.revisions.values().next().unwrap().git_ref.clone()
    }
}

#[derive(Clone)]
pub struct Client {}

impl Client {
    pub fn new() -> Client {
        Client {}
    }

    // gets the latest revision ref for a given change
    pub async fn get_latest_revision_ref(&self, change: usize) -> Result<String> {
        let request_url = format!("https://gerrit.wikimedia.org/r/changes/{}?o=CURRENT_REVISION", change);
        let response = reqwest::get(&request_url)
          .await.with_context(|| "failed http request to gerrit")?;
        let body = response.text()
          .await.with_context(|| "failed to read response body from gerrit")?;
        let mut lines= body.lines();
        lines.next();
        let json_str = lines.next().unwrap();
        let gerrit_change: Change = serde_json::from_str(json_str).unwrap(); 
        Ok(gerrit_change.git_ref())
    }

    // gets all open patches for a given project
    pub async fn get_open_patches(&self, project: &str) -> Result<Vec<Change>> {
        let request_url = format!("https://gerrit.wikimedia.org/r/changes/?q=project:{}+status:open&o=CURRENT_REVISION", project);
        let response = reqwest::get(&request_url)
          .await.with_context(|| "failed http request to gerrit")?;
        let body = response.text()
          .await.with_context(|| "failed to read response body from gerrit")?;
        let mut lines= body.lines();
        lines.next();
        let json_str = lines.next().unwrap();
        let open_patches: Vec<Change> = serde_json::from_str(json_str).unwrap();
        Ok(open_patches)
    }

    // gets all closed patches for a given project
    pub async fn get_closed_patches(&self, project: &str) -> Result<Vec<Change>> {
        let request_url = format!("https://gerrit.wikimedia.org/r/changes/?q=project:{}+status:closed&o=CURRENT_REVISION", project);
        let response = reqwest::get(&request_url)
          .await.with_context(|| "failed http request to gerrit")?;
        let body = response.text()
          .await.with_context(|| "failed to read response body from gerrit")?;
        let mut lines= body.lines();
        lines.next();
        let json_str = lines.next().unwrap();
        let closed_patches: Vec<Change> = serde_json::from_str(json_str).unwrap(); 
        Ok(closed_patches)
    }

}