use askama::Template;
use axum::http::StatusCode;
use axum::{extract::Form, extract::State, routing::{get}, Router, Json};
use chrono::{DateTime, Utc};
use std::collections::HashMap;
use std::{net::SocketAddr};
use serde::{Serialize,Deserialize};
use tower_http::trace::{self, TraceLayer};
use tracing::Level;
use std::sync::{Arc};
use tokio::sync::{RwLock};

pub mod gerrit;
pub mod gitlab;


#[derive(Clone)]
struct AppState {
    gitlab_client: gitlab::Client,
    gerrit_client: gerrit::Client,
}

type SharedState = Arc<RwLock<AppState>>;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt()
       .with_target(false)
       .pretty()
       .init();

    let gitlab_client = gitlab::Client::new(
        &std::env::var("GITLAB_API_TOKEN").unwrap(),
        &std::env::var("GITLAB_TRIGGER_TOKEN").unwrap()
    );
    let gerrit_client = gerrit::Client::new();
    let shared_state: SharedState = Arc::new(RwLock::new(AppState { gitlab_client, gerrit_client }));

    let app = Router::new()
        .route("/", get(show_form).post(accept_form))
        .route("/api/v0/environments", get(get_environments))
        .route("/api/v0/environments/stop_old", get(stop_old_environments))
        .route("/api/v0/environments/start_new", get(start_new_environments))
        .route("/api/v0/environments/update_stale", get(update_stale_environments))
        .layer(
            TraceLayer::new_for_http()
                .make_span_with(trace::DefaultMakeSpan::new().level(Level::INFO))
                .on_response(trace::DefaultOnResponse::new().level(Level::INFO)),
        )
        .with_state(Arc::clone(&shared_state));

    let port_str = std::env::var("PORT").unwrap_or("8000".to_string());
    let port = port_str.parse::<u16>().unwrap();
    let addr = SocketAddr::from(([0, 0, 0, 0], port));
    tracing::info!("listening on http://{}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

#[derive(Template)]
#[template(path = "integration_new.html")]
struct IntegrationEditTemplate {  }

async fn show_form() -> IntegrationEditTemplate  {
    IntegrationEditTemplate{}
}

#[derive(Deserialize, Debug)]
struct IntegrateInput {
    number: usize,
    action: String,
}

#[derive(Template)]
#[template(path = "integration_show.html")]
struct IntegrationShowTemplate { 
    change_number: usize,
    action: String,
    latest_ref: String,
    pipeline_url: String,
 }

async fn accept_form(State(state): State<SharedState>, Form(input): Form<IntegrateInput>) -> IntegrationShowTemplate {
    let latest_ref = state.read().await.gerrit_client.get_latest_revision_ref(
        input.number
    ).await.unwrap();
    let pipeline_url = state.read().await.gitlab_client.trigger_pipeline(
        input.number, &input.action, &latest_ref
    ).await.unwrap();
    IntegrationShowTemplate {
        change_number: input.number,
        latest_ref: latest_ref,
        pipeline_url: pipeline_url,
        action: input.action,
    }
}

async fn get_environments(State(state): State<SharedState>) -> Result<Json<Vec<Environment>>, StatusCode> {
    let shared_state = &mut *state.write().await;
    let client = &mut shared_state.gitlab_client;
    let tracker = match EnvironmentTracker::wrangle_environments(client).await {
        Ok(tr) => tr,
        Err(err) => {
            tracing::error!("{:?}", err);
            return Err(axum::http::StatusCode::INTERNAL_SERVER_ERROR);
        }
    };
    let mut envs: Vec<Environment> = tracker.environments.values().map(|v| v.to_owned()).collect();
    envs.sort_by(|a, b| a.last_action_time.cmp(&b.last_action_time));
    Ok(Json(envs))
}

// gets the list of closed patches from gerrit, compares it to the list of
// environments we have that are running, and triggers a stop pipeline for each thereof
async fn stop_old_environments(State(state): State<SharedState>) -> Result<Json<Vec<Environment>>, StatusCode> {
    // get list of running environments
    let shared_state = &mut *state.write().await;
    let client = &mut shared_state.gitlab_client;
    let tracker = match EnvironmentTracker::wrangle_environments(client).await {
        Ok(tr) => tr,
        Err(err) => {
            tracing::error!("{:?}", err);
            return Err(axum::http::StatusCode::INTERNAL_SERVER_ERROR);
        }
    };
    let running_environments = tracker.running_environments();
    // get list of closed patches from gerrit
    let closed_patches = shared_state.gerrit_client.get_closed_patches("mediawiki/extensions/WikiLambda").await.unwrap();
    let closed_change_numbers: Vec<usize> = closed_patches.iter().map(|p| p.number).collect();
    // get patches closed and running
    let mut envs_to_stop: Vec<Environment> = running_environments.iter().filter(|env| {
        closed_change_numbers.contains(&env.change_number)
    }).map(|env| env.to_owned()).collect();
    // stop each of them
    for env in envs_to_stop.iter_mut() {
        let pipeline_url = client.trigger_stop( env.change_number).await.unwrap();
        env.pipeline_urls.push(pipeline_url.clone());
        tracing::info!("triggered stop pipeline for change {} at {}", env.change_number, pipeline_url);
    }
    Ok(Json(envs_to_stop))
}

// triggers start pipelines for open patches whose ref is newer than the one we have running
async fn update_stale_environments(State(state): State<SharedState>) -> Result<Json<Vec<Environment>>, StatusCode> {
    // get list of running environments
    let shared_state = &mut *state.write().await;
    let client = &mut shared_state.gitlab_client;
    let tracker = match EnvironmentTracker::wrangle_environments(client).await {
        Ok(tr) => tr,
        Err(err) => {
            tracing::error!("{:?}", err);
            return Err(axum::http::StatusCode::INTERNAL_SERVER_ERROR);
        }
    };
    let known_environments = tracker.all_environments();
    // get list of open patches from gerrit
    let open_patches = shared_state.gerrit_client.get_open_patches("mediawiki/extensions/WikiLambda").await.unwrap();
    let open_change_numbers_and_latest_ref: HashMap<usize, String> = open_patches.iter().map(|p| (p.number, p.git_ref())).collect();
    // get patches open with the last deployed environment not matching the latest ref
    let mut envs_to_update: Vec<Environment> = known_environments.iter().filter(|env| {
        open_change_numbers_and_latest_ref.iter().any(|(num, ref_)| {
            num == &env.change_number && ref_ != &env.latest_ref
        })
    }).map(|env| env.to_owned()).collect();
    // trigger start pipeline for each of them
    for env in envs_to_update.iter_mut() {
        let new_ref = open_change_numbers_and_latest_ref.get(&env.change_number).unwrap();
        let pipeline_url = client.trigger_run(env.change_number, new_ref).await.unwrap();
        env.pipeline_urls.push(pipeline_url.clone());
        tracing::info!("triggered start pipeline for change {} at {}", env.change_number, pipeline_url);
    }
    Ok(Json(envs_to_update))
}

// gets list of open patches from gerrit, and starts new envionments for each one that doesn't have one
async fn start_new_environments(State(state): State<SharedState>) -> Result<Json<Vec<Environment>>, StatusCode> {
    // get list of running environments
    let shared_state = &mut *state.write().await;
    let client = &mut shared_state.gitlab_client;
    let tracker = match EnvironmentTracker::wrangle_environments(client).await {
        Ok(tr) => tr,
        Err(err) => {
            tracing::error!("{:?}", err);
            return Err(axum::http::StatusCode::INTERNAL_SERVER_ERROR);
        }
    };
    let known_environments = &tracker.all_environments();
    // get list of open patches from gerrit
    let open_patches = shared_state.gerrit_client.get_open_patches("mediawiki/extensions/WikiLambda").await.unwrap();
    let open_change_numbers_and_latest_refs: Vec<(usize,String)> = open_patches.iter().map(|p| (p.number, p.git_ref().clone())).collect();
    // get patches open with no corresponding environment
    let mut envs_to_start: Vec<Environment> = open_change_numbers_and_latest_refs.iter().filter(|(change_number, _)| {
        !known_environments.iter().any(|env| env.change_number == *change_number)
    }).map(|(change_number, latest_ref)| {
        Environment {
            change_number: *change_number,
            latest_ref: latest_ref.clone(),
            pipeline_urls: vec![],
            last_action_time: Utc::now(),
            status: EnvironmentStatus::Running,
        }
    }).collect();
    // start each of them
    for env in envs_to_start.iter_mut() {
        let pipeline_url = client.trigger_run( env.change_number, &env.latest_ref).await.unwrap();
        env.pipeline_urls.push(pipeline_url.clone());
        tracing::info!("triggered pipeline for change {} at {}", env.change_number, pipeline_url);
    }
    Ok(Json(envs_to_start))
}

struct EnvironmentTracker {
    environments: HashMap<usize, Environment>
}

enum EnvironmentPipelineAction {
    Run,
    Stop,
}
impl EnvironmentPipelineAction {
    fn to_environment_status(&self) -> EnvironmentStatus {
        match self {
            Self::Run => EnvironmentStatus::Running,
            Self::Stop => EnvironmentStatus::Stopped,
        }
    }
}
struct EnvironmentPipelineEvent {
    change_number: usize,
    action: EnvironmentPipelineAction,
    gerrit_ref: String,
    pipeline_url: String,
    time: DateTime<Utc>
}

#[derive(Serialize, Clone, Eq, PartialEq, Hash)]
enum EnvironmentStatus {
    Running,
    Stopped,
}

#[derive(Serialize, Clone)]
struct Environment { 
    change_number: usize,
    latest_ref: String,
    pipeline_urls: Vec<String>,
    last_action_time: DateTime<Utc>,
    status: EnvironmentStatus,
}

impl EnvironmentTracker {
   fn new() -> EnvironmentTracker {
    EnvironmentTracker { environments: HashMap::new() }
   }

   async fn wrangle_environments(client: &mut gitlab::Client) -> Result<EnvironmentTracker, anyhow::Error> {
    let mut tracker = Self::new();
    let pipelines = match client.get_pipelines().await {
        Ok(pipelines) => pipelines,
        Err(err) => { anyhow::bail!("error getting pipelines: {:?}", err); }
    };
    for pipeline in pipelines {
        let mut gerrit_ref: String = "".to_owned();
        let mut change_number: usize = 0;
        let mut action: EnvironmentPipelineAction = EnvironmentPipelineAction::Run;
        for variable in &pipeline.variables {
            if variable.key == "GERRIT_CHANGE_NUMBER" {
                change_number = variable.value.parse::<usize>().unwrap_or(0);
            } else if variable.key == "WIKILAMBDA_REF" {
                gerrit_ref = variable.value.to_string();
            } else if variable.key == "ACTION" {
                if variable.value == "stop" {
                    action = EnvironmentPipelineAction::Stop;
                }
            }
        }
        tracker.process_event(&EnvironmentPipelineEvent{
            change_number,
            action,
            gerrit_ref,
            pipeline_url: pipeline.web_url,
            time: pipeline.created_at,
        });
    };
    Ok(tracker)
   }

   // returns a list of running environments
   fn running_environments(&self) -> Vec<Environment> {
    self.environments.values().filter(|env| env.status == EnvironmentStatus::Running).map(|env| env.to_owned()).collect()
   }

   fn all_environments(&self) -> Vec<Environment> {
    self.environments.values().map(|env| env.to_owned()).collect()
   }

   fn process_event(&mut self, event: &EnvironmentPipelineEvent) {
    if let Some(env) = self.environments.get_mut(&event.change_number) {
        env.pipeline_urls.push(event.pipeline_url.to_owned());
        if event.time > env.last_action_time {
            env.last_action_time = event.time;
            env.latest_ref = event.gerrit_ref.to_owned();
            env.status = event.action.to_environment_status();
        }
    } else {
        self.environments.insert(event.change_number, Environment{ 
            change_number: event.change_number,
            latest_ref: event.gerrit_ref.to_owned(),
            pipeline_urls: vec!(event.pipeline_url.to_owned()),
            last_action_time: event.time,
            status: event.action.to_environment_status(),
        });
    } 
   }
}

